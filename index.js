$(document).ready(function() {
	var width = $(document).width();

	var aboutPadding = Math.round(width * 0.0275) + "";
	$(".about-me p").css({"padding":"0px 0px 0px " + aboutPadding + "px"});

	var portPadding = Math.round(width * 0.355) + "";
	$(".portfolio h1").css({"padding": "80px " + portPadding + "px 0px " + portPadding + "px"});
	$(".portfolio h3").css({"padding": "0px " + portPadding + "px 0px " + portPadding + "px"});
	$(".portfolio ul").css({"padding":"0px " + portPadding + "px 200px " + portPadding + "px"});
});
